package com.anil;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author anilg
 */
public final class SpringBeanFactory {
	/**
	 * The singleton instance
	 */
	private static SpringBeanFactory instance = null;
	/**
	 * The Spring based Application Context
	 */
	private static ApplicationContext m_context = null;
	/**
	 * private constructor
	 */
	private SpringBeanFactory() {
		m_context = new ClassPathXmlApplicationContext("beans.xml");
	}
	/**
	 * @return
	 */
	public static SpringBeanFactory getInstance() {
		
		if(instance == null) {
			instance = new SpringBeanFactory();
		}
		
		if(m_context == null )  {
			throw new IllegalStateException("Spring Bean Factory is not initialized.");
		}
		return instance;
	}
	/**
	 * @param <T>
	 * @param beanName
	 * @param requiredType
	 * @return
	 * @throws BeansException
	 */
	public <T> T getBean(String beanName,
						 Class<T> requiredType) throws BeansException {
		return m_context.getBean(beanName, requiredType);
	}
}
